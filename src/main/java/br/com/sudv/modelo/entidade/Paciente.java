package br.com.sudv.modelo.entidade;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "tb_pacientes")
@Data
@Getter
@Setter
public class Paciente {

    @Id
    public int idPaciente;

    private String nomePaciente;

    @Column(name = "cpf_paciente")
    private String cpf;
    private String senhaPaciente;
    private LocalDate dtNascimento;
    private String cns;
    private String nomeSocial;
    private String generoPaciente;
    private String cepPaciente;
    private String ruaPaciente;

    @Column(name = "numero_casa_paciente")
    private String numeroCasa;

    @Column(name = "complemento_end")
    private String complemento;

    private String bairroPaciente;
    private String cidadePaciente;
    private String estadoPaciente;
    private String telefonePaciente;
    private Character isResponsavel;
    private Character isDependente;

    public int getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(int idPaciente) {
        this.idPaciente = idPaciente;
    }

    public String getNomePaciente() {
        return nomePaciente;
    }

    public void setNomePaciente(String nomePaciente) {
        this.nomePaciente = nomePaciente;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getSenhaPaciente() {
        return senhaPaciente;
    }

    public void setSenhaPaciente(String senhaPaciente) {
        this.senhaPaciente = senhaPaciente;
    }

    public LocalDate getDtNascimento() {
        return dtNascimento;
    }

    public void setDtNascimento(LocalDate dtNascimento) {
        this.dtNascimento = dtNascimento;
    }

    public String getCns() {
        return cns;
    }

    public void setCns(String cns) {
        this.cns = cns;
    }

    public String getNomeSocial() {
        return nomeSocial;
    }

    public void setNomeSocial(String nomeSocial) {
        this.nomeSocial = nomeSocial;
    }

    public String getGeneroPaciente() {
        return generoPaciente;
    }

    public void setGeneroPaciente(String generoPaciente) {
        this.generoPaciente = generoPaciente;
    }

    public String getCepPaciente() {
        return cepPaciente;
    }

    public void setCepPaciente(String cepPaciente) {
        this.cepPaciente = cepPaciente;
    }

    public String getRuaPaciente() {
        return ruaPaciente;
    }

    public void setRuaPaciente(String ruaPaciente) {
        this.ruaPaciente = ruaPaciente;
    }

    public String getNumeroCasa() {
        return numeroCasa;
    }

    public void setNumeroCasa(String numeroCasa) {
        this.numeroCasa = numeroCasa;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairroPaciente() {
        return bairroPaciente;
    }

    public void setBairroPaciente(String bairroPaciente) {
        this.bairroPaciente = bairroPaciente;
    }

    public String getCidadePaciente() {
        return cidadePaciente;
    }

    public void setCidadePaciente(String cidadePaciente) {
        this.cidadePaciente = cidadePaciente;
    }

    public String getEstadoPaciente() {
        return estadoPaciente;
    }

    public void setEstadoPaciente(String estadoPaciente) {
        this.estadoPaciente = estadoPaciente;
    }

    public String getTelefonePaciente() {
        return telefonePaciente;
    }

    public void setTelefonePaciente(String telefonePaciente) {
        this.telefonePaciente = telefonePaciente;
    }

    public Character getIsResponsavel() {
        return isResponsavel;
    }

    public void setIsResponsavel(Character isResponsavel) {
        this.isResponsavel = isResponsavel;
    }

    public Character getIsDependente() {
        return isDependente;
    }

    public void setIsDependente(Character isDependente) {
        this.isDependente = isDependente;
    }
}
