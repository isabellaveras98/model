package br.com.sudv.modelo.entidade;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "tb_enfermeiros")
@Data
@Getter
@Setter
public class Enfermeiro {

    @Id
    public int idEnfermeiro;
    public String nomeEnfermeiro;

    @Column(name = "nm_coren")
    public String coren;

    @Column(name = "login_enfermeiro")
    public String login;

    @Column(name ="senha_enfermeiro")
    public String senha;

}
