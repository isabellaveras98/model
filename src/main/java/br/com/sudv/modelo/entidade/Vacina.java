package br.com.sudv.modelo.entidade;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "tb_vacina")
@Data
public class Vacina {

    @Id
    int idVacina;

    String nomeVacina;
    String lote;
    LocalDate dtValidade;
    String fornecedor;

    @Column(name = "nome_campanha")
    String campanha;


}
