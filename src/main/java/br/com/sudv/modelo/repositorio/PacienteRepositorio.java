package br.com.sudv.modelo.repositorio;

import br.com.sudv.modelo.entidade.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PacienteRepositorio extends JpaRepository<Paciente, Integer> {


    Paciente findByCpfOrCns(String cpf, String cns);
}

