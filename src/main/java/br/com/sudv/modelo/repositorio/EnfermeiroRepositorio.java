package br.com.sudv.modelo.repositorio;

import br.com.sudv.modelo.entidade.Enfermeiro;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EnfermeiroRepositorio extends JpaRepository<Enfermeiro, Integer> {


      List<Enfermeiro> findByCoren(String coren);
}
