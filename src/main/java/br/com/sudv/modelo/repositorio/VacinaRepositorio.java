package br.com.sudv.modelo.repositorio;

import br.com.sudv.modelo.entidade.Vacina;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VacinaRepositorio extends JpaRepository<Vacina, Integer> {

    List<Vacina> findByLote(String lote);

}
